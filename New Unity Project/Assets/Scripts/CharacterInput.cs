﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacterInput : MonoBehaviour
{
    public int count = 0;
    private Character _character;

    private void Awake()
    {
        _character = GetComponent<Character>();
    }
    // Start is called before the first frame update
    public void Start()
    {
    }

    public void Update()
    {
        _character.Move();
        _character.Jump();
        Special();
        Item();
        ChangeCharacter();
    }

    // Use your special attack
    public virtual void Special()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            _character.Special();
        }
    }
    // To pick up the item which give stats boost
    public virtual void Item()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            _character.Item();
        }
    }
    public void ChangeCharacter()
    {
        // Switch to change character
        if (Input.GetKeyDown(KeyCode.R))
        {
            switch (count)
            {
                case 0:
                    _character.character = GetComponent<Dragon>();
                    _character.character.EnterCharacter();
                    count++;
                    break;
                case 1:
                    _character.character = GetComponent<Mage>();
                    _character.character.EnterCharacter();
                    count++;
                    break;
                case 2:
                    _character.character = GetComponent<Warlock>();
                    _character.character.EnterCharacter();
                    count = 0;
                    break;
            }
        }
    }

 
}
