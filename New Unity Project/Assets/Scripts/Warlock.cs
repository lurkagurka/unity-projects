﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Warlock : Character
{
    private readonly string _special = "Firesoul";
    private readonly string _item = "Death Ring";
    public override void EnterCharacter()
    {
        // Sets color and stats for the Dragon
        baseMoveSpeed = 2;
        baseJumpForce = 0;
        Debug.Log("Entered Warlock");
        moveSpeed = baseMoveSpeed;
        jumpForce = baseJumpForce;
        special = _special;
        item = _item;
    }

    protected override void Start()
    {
    }

    public override void Move()
    {
        base.Move();
    }

    public override void Jump()
    {
        base.Jump();
    }

    public override void Special()
    {
        base.Special();
    }

    public override void Item()
    {
        base.Item();
    }
}
