﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public delegate void CharacterMovementHandler();
public class Character : MonoBehaviour
{
    private Rigidbody _rb;
    private CharacterInput characterInput;
    public Character character;
    public bool grounded;
    private Dragon _dragon;
    private Mage _mage;
    private Warlock _warlock;
    public static event CharacterMovementHandler PlayerJumping;
    public static event CharacterMovementHandler PlayerMoving;
    public static event CharacterMovementHandler PlayerNotJumping;
    public static event CharacterMovementHandler PlayerNotMoving;
    
    protected string special = "none";
    protected string baseItem = "none";
    protected string item = "none";
    public float moveSpeed;
    public float baseMoveSpeed;
    public float jumpForce;
    public float baseJumpForce;
    
    // Sets material, CharacterInput script and rigidbody
    public void Awake()
    {
        // Instantiates objects
        _rb = GetComponent<Rigidbody>();
        characterInput = GetComponent<CharacterInput>();
    }

    protected virtual void Start()
    {
        character = GetComponent<Character>();
    }
    public void Update()
    {
        
    }

    
    // What happens when entering the character
    public virtual void EnterCharacter()
    {
        
    }
    
    // Movement function
    public virtual void Move()
    {
        Vector3 movement = new Vector3(Input.GetAxis("Horizontal"), 0f, Input.GetAxis("Vertical"));
        transform.position += movement * (Time.deltaTime * moveSpeed);
        
            if (movement != Vector3.zero)
            {

                if (PlayerMoving != null)
                {
                    PlayerMoving();
                }
            }
            else 
            {
                if (PlayerNotMoving != null)
                {
                   PlayerNotMoving();
                }
            }
    }
    
    //Jump function
    public virtual void Jump()
    {
        //jumping
        if (Input.GetKeyDown(KeyCode.Space) && grounded)
        {
            _rb.velocity = new Vector3(_rb.velocity.x, character.jumpForce, _rb.velocity.z);
            grounded = false;
            if (PlayerJumping != null)
            {
                PlayerJumping();
            }
        }
        
    }
    // Special ability
    public virtual void Special()
    {
        {
            Debug.Log(character.special);
        }
    }
    public virtual void Item()
    {
        Debug.Log(character.item);
        
    }

    public void OnCollisionEnter(Collision other)
    {
        grounded = true;
        if (PlayerNotJumping != null)
        {
            PlayerNotJumping();
        }
        
    }

    // Pick up item
    
}
