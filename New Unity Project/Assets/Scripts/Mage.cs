﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mage : Character
{
    private string _special = "Fireball";
    private readonly string _item = "Mana Ring";
    public override void EnterCharacter()
    {
        // Sets color and stats for the Dragon
        baseMoveSpeed = 3;
        baseJumpForce = 3;
        Debug.Log("Entered Mage");
        moveSpeed = baseMoveSpeed;
        jumpForce = baseJumpForce;
        special = _special;
        item = _item;
    }
    protected override void Start()
    {
    }

    public override void Move()
    {
        base.Move();
    }

    public override void Jump()
    {
        base.Jump();
    }

    public override void Special()
    {
        base.Special();
    }

    public override void Item()
    {
        base.Item();
    }
}
