﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EventHandler : MonoBehaviour
{
    public Image JumpingButton;
    public Image NotJumpingButton;
    public Image MovingButton;
    public Image NotMovingButton;
    void Start()
    {
        Character.PlayerJumping += Jumping;
        Character.PlayerNotJumping += NotJumping;
        Character.PlayerMoving += Moving;
        Character.PlayerNotMoving += NotMoving;
    }

    public void Jumping()
    {
        JumpingButton.color = Color.green;
        NotJumpingButton.color = Color.white;
    }
    public void NotJumping()
    {
        NotJumpingButton.color = Color.red;
        JumpingButton.color = Color.white;
    }
    public void Moving()
    {
        MovingButton.color = Color.green;
        NotMovingButton.color = Color.white;
    }
    public void NotMoving()
    {
        NotMovingButton.color = Color.red;
        MovingButton.color = Color.white;
    }
}
