﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dragon : Character
{
    private string _special = "Firebreath";
    private readonly string _item = "Fire Ring";
    public override void EnterCharacter()
    {
        // Sets color and stats for the Dragon
        baseMoveSpeed = 10;
        baseJumpForce = 10;
        Debug.Log("Entered Dragon");
        moveSpeed = baseMoveSpeed;
        jumpForce = baseJumpForce;
        special = _special;
        item = _item;
    }
    protected override void Start()
    {
    }

    public override void Move()
    {
        base.Move();
    }

    public override void Jump()
    {
        base.Jump();
    }

    public override void Special()
    {
        base.Special();
    }

    public override void Item()
    {
        base.Item();
    }
}
